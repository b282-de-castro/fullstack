import Banner from '../components/Banner';

export default function Error() {

    const data = {
        title: "Error 404 - Page not found.",
        content: "The page you are looking for cannot be found.",
        destination: "/",
        label: "Back to Home"
    }

    return (
        <Banner data={data} />
    )
}

/*
actvity code
import Banner from '../components/Banner';

const Error = () => {
  return (
    <div>
      <Banner
        title="Error 404 - Page Not Found"
        description="The page you are looking for cannot be found."
        buttonText="Go back to Home"
        buttonLink="/"
      />
    </div>
  );
};

export default Error;

*/