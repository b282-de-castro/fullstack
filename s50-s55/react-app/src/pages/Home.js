import Banner from '../components/Banner';
import Highlights from '../components/Highlights';


export default function Home() {

  const data = {
    title: "Zuitt Coding Bootcamp",
    content: "Opportunities for everyone, everywhere.",
    destination: "/courses",
    label: "Enroll now!"
  }


  return (
    <>
    <Banner data={data} />
      <Highlights />
      
    </>
  )
}




/*
export default function Home(){
	return (

		<>
	      <Banner />
	      <Highlights />

		</>
		)
}



//activity code

const Home = () => {
  return (
    <>
      <Banner
        title="Zuitt Coding Bootcamp"
        description="Opportunities for everyone, everywhere."
        buttonText="Enroll now!"
        buttonLink="/courses"
      />
      <Highlights />
    </>
  );
};

export default Home;


*/