//console.log("hello world!")

const txtFirstName = document.querySelector("#txt-first-name");
const txtLastName = document.querySelector("#txt-last-name");
const spanFullName = document.querySelector("#span-full-name");

txtFirstName.addEventListener("keyup",FullName);
txtLastName.addEventListener("keyup",FullName);

function FullName(){
	const firstName = txtFirstName.value;
	const lastName = txtLastName.value;

	spanFullName.innerHTML = `${firstName} ${lastName}`
	console.log(firstName);
	console.log(lastName);
};